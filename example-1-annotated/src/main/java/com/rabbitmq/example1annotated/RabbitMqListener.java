package com.rabbitmq.example1annotated;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Created by dshv on 22.06.2015.
 */
@EnableRabbit
@Component
public class RabbitMqListener {
    private static Logger logger = LoggerFactory.getLogger(RabbitConfiguration.class);

    @RabbitListener(queues = "queue1")
    public void processQueue1(String message) {
        logger.info("Received from queue 1: " + message);
    }

}
